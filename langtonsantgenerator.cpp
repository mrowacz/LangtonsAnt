#include "langtonsantgenerator.h"
#include <QTime>
#include <QDebug>
#include <QPainter>

LangtonsAntGenerator::LangtonsAntGenerator()
{
    h = 200;
    w = 200;
    QTime time = QTime::currentTime();
    qsrand((uint)time.msec());
}

void LangtonsAntGenerator::addAnt(int posX, int posY, QRgb color)
{
    antV.push_back(new Ant(pixMap, w, h, color));
    antV[antV.size()-1]->setPos(posX, posY);
}

int LangtonsAntGenerator::randomInt(int min, int max)
{
    return qrand() % ((max + 1) - min) + min;
}

void LangtonsAntGenerator::addAntRandom(QRgb color)
{
    int posX = randomInt(0, w - 1);
    int posY = randomInt(0, h - 1);
    antV.push_back(new Ant(pixMap, w, h, color));
    antV[antV.size()-1]->setPos(posX, posY);
}

void LangtonsAntGenerator::generate()
{
    for (int i = 0; i <iterations; i++) {
        for (unsigned int j = 0; j < antV.size(); j++)
            antV[j]->move();
    }
}

void LangtonsAntGenerator::init()
{
    pixMap = new unsigned int *[h];

    for (int i = 0; i < h; i++) {
        pixMap[i] = new unsigned int [w];
        std::fill(pixMap[i], pixMap[i] + w, 0);
    }
}

void LangtonsAntGenerator::addNoise(int noiseNum)
{
    for (int i = 0; i < noiseNum; i++)
        pixMap[randomInt(0, h-1)][randomInt(0, w-1)] = qRgb(1, 1, 1);
}

QImage LangtonsAntGenerator::getImage(int wNew, int hNew)
{
    qDebug() << "Image params [" << wNew << "," << hNew << "]";

    QImage img(w, h, QImage::Format_RGB32);
    img.fill(QColor(Qt::white).rgb());
    for (int i = 0; i < h; i++)
        for (int j = 0; j < w; j++)
            if (pixMap[i][j] != 0)
                img.setPixel(j, i, pixMap[i][j]);
    return img.scaled(wNew, hNew, Qt::KeepAspectRatio);
}
