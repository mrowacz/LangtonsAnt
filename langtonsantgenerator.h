#ifndef LANGTONSANTGENERATOR_H
#define LANGTONSANTGENERATOR_H

#include <iostream>
#include <vector>
#include <ant.h>

#include <QImage>
#include <QPainter>

class LangtonsAntGenerator
{
public:
    LangtonsAntGenerator();

    static QRgb antColors[];

    void setIterations(int v) { iterations = v; }
    void setW(int w) { this->w = w; }
    void setH(int h) { this->h = h; }

    void addAnt(int posX, int posY, QRgb color);
    void addAntRandom(QRgb color);
    void addNoise(int noiseNum);
    void generate();
    void init();
    QImage getImage(int wNew, int hNew);


private:
    int randomInt(int min, int max);

    int w;
    int h;
    unsigned int **pixMap;
    int iterations;

    QImage *img;
    std::vector<Ant *> antV;
};

#endif // LANGTONSANTGENERATOR_H
