#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>
#include <QDebug>
#include <QTimer>
#include <QTime>
#include <QPainter>
#include <QPaintEvent>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    int antNum = 120;
    QTime time = QTime::currentTime();
    qsrand((uint)time.msec());

    auto rndFunc = []()->int{ return qrand() % (255 + 1); };

    langG.setW(900);
    langG.setH(900);
    langG.setIterations(10000);
    langG.init();

    for (int i = 0; i < antNum; i++)
        langG.addAntRandom(qRgb(rndFunc(), rndFunc(), rndFunc()));
    langG.addNoise(25000);
    langG.generate();

    // enable ui
    ui->setupUi(this);

    // start
    QTimer::singleShot(100, this, SLOT(repaint()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QMainWindow::paintEvent(event);
    QPainter p(this);

    p.drawImage(0, 0, langG.getImage(this->geometry().width(),
                                     this->geometry().height()));
}
