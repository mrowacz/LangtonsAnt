#ifndef ANT_H
#define ANT_H

#include <mutex>
#include <direction.h>

#include <QPainter>

class Ant
{
public:
    Ant(unsigned int **m, int w, int h, QRgb color);

    void setPos(int x, int y);
    void move();

protected:
    Ant();
    void transpose();

private:
    unsigned int **mm;
    int w;
    int h;

    int x = 0;
    int y = 0;
    QRgb color;
    Directions dir;
};

#endif // ANT_H
