#ifndef DIRECTION_H
#define DIRECTION_H

enum Directions {
    upPos, rightPos, downPos, leftPos
};

class Direction
{    
public:
    static Direction &getInstance() {
        static Direction directionInstance;
        return directionInstance;
    }
    Direction(Direction const &) = delete;
    Direction(Direction &&) = delete;
    Direction& operator=(Direction const &) = delete;
    Direction& operator=(Direction &&) = delete;

    Directions moveLeft(Directions dir)
    {
        DoubleLinkDir *d = &dirDList[0];
        while(d->value != dir)
            d = d->next;
        return d->prev->value;
    }

    Directions moveRight(Directions dir)
    {
        DoubleLinkDir *d = &dirDList[0];
        while(d->value != dir)
            d = d->next;
        return d->next->value;
    }

protected:
    struct DoubleLinkDir {
        Directions value;
        DoubleLinkDir *next;
        DoubleLinkDir *prev;
    };
    DoubleLinkDir dirDList[4];

    Direction();

    ~Direction();
};

#endif // DIRECTION_H
