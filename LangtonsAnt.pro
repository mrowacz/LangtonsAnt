#-------------------------------------------------
#
# Project created by QtCreator 2017-03-09T12:42:30
#
#-------------------------------------------------

QT       += core gui
CONFIG   += c++11
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = LangtonsAnt
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    ant.cpp \
    direction.cpp \
    langtonsantgenerator.cpp

HEADERS  += mainwindow.h \
    ant.h \
    direction.h \
    langtonsantgenerator.h

FORMS    += mainwindow.ui
