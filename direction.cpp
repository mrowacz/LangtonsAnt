#include "direction.h"

Direction::Direction() {
    dirDList[0].next = &dirDList[1];
    dirDList[1].next = &dirDList[2];
    dirDList[2].next = &dirDList[3];
    dirDList[3].next = &dirDList[0];

    dirDList[0].prev = &dirDList[3];
    dirDList[1].prev = &dirDList[0];
    dirDList[2].prev = &dirDList[1];
    dirDList[3].prev = &dirDList[2];

    dirDList[0].value = upPos;
    dirDList[1].value = rightPos;
    dirDList[2].value = downPos;
    dirDList[3].value = leftPos;
}

Direction::~Direction() {

}
