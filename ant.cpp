#include "ant.h"
#include <QDebug>

Ant::Ant(unsigned int **m, int w, int h, QRgb color)
{
    this->w = w;
    this->h = h;
    this->mm = m;

    this->color = color;
    this->dir = Directions::upPos;

    qDebug() << "Create ANT field" << w << " " << h;
}

void Ant::setPos(int x, int y)
{
    this->x = x;
    this->y = y;
}

void Ant::move()
{
    int v = mm[y][x];

    // if white(0) - not visited
    if (v == 0) {
        dir = Direction::getInstance().moveLeft(dir);
        mm[y][x] = color;
    }
    else {
        dir = Direction::getInstance().moveRight(dir);
        mm[y][x] = 0;
    }
    transpose();
}

void Ant::transpose()
{
    int oldX = x;
    int oldY = y;
    switch(dir) {
    case Directions::upPos:
        y = oldY == 0 ? h - 1 : --oldY;
        break;
    case Directions::rightPos:
        x = oldX == w - 1 ? 0 : ++oldX;
        break;
    case Directions::downPos:
        y = oldY == h - 1 ? 0 : ++oldY;
        break;
    case Directions::leftPos:
        x = oldX == 0 ? w - 1 : --oldX;
        break;
    default:
        break;
    }
}
